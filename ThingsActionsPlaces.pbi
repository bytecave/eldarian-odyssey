﻿DataSection
  __NounList__:
  Data.s "BOTTLE,!DIG,!SEARCH,*0100-10000000,$WOODSBURIED,[0]It's a hard glass bottle\@ but most of it is buried. You'll have to dig it up."
  Data.s "POTION,^ELIXIR,!DRINK,!IMBIBE,!DRAIN,!QUAFF,!SIP,*0000-10000000,$WOODSBURIED,[0]Effervescent\@ green liquid swirls inside. Drinking it will heal you of damage."
  Data.s "GATE,^BARRIER,!OPEN,!CLIMB,!KNOCK,!BANG,!CLOSE,!BURN,!UNLOCK,!PUSH,*1101-11000000,$ZARBURGSOUTHGATE,@KNOCK:+3+4,@KNOCK:+6:$ZARBURGSOUTHGATE,[0]It's a large\@ rough-hewn log gate with a sign reading 'South Zarburg.'[1] The gate is bolted closed[3]\@ but a small sliding window is open. You can see a watchman looking at you[1].[2] The gate heading into Zarburg is currently open."
  Data.s "AXE,^AX,!USE,!SWING,*1001-10000000,$ZARBURGSOUTHGATE,@GET:-4:$ZARBURGSOUTHGATE,[0]It's a beautiful hand-crafted axe with a very sharp blade.[1]The axe is broken and lodged firmly into the fallen tree\@ its blade twisted and chipped."
  Data.s "OAK,&TREE,!CLIMB,!CHOP,!CUT,!DESCEND,!DOWN,!BURN,*1101-10000000,$ZARBURGSOUTHGATE,@CHOP:-0+1-A+F:^AXE,@CHOP:-3+5:$ZARBURGSOUTHGATE,@CHOP:-0+1,[0]It's a tall oak tree with broad\@ strong branches.[1]This once mighty oak now lies in front of the Zarburg South gate\@ a twisted axe embedded in it trunk."
  Data.s "BIRCH,&TREE,!CLIMB,!DESCEND,!CHOP,!BURN,*1100-10000000,$WOODSBURIED,[0]Something is mostly buried near the base of the birch.[1]Something used to be buried near the base of this tree."
  Data.s "FIR,&TREE,!CLIMB,!DESCEND,!CHOP,!BURN,*1100-10000000,$WOODSTREE,[0]This fir tree stands alone. It is much wider And seems to be taller than any other tree you've seen in the woods."
  Data.s "CLIFF,^PRECIPICE,!CLIMB,!DESCEND,*1101-10000000,$ZARBURGCLIFF,[0]The cliff over-hang is very treacherous. One false step and you will fall!"
  Data.s "FISH,!CATCH,!EAT,!THROW,!TOSS,!KISS,!SPEAK,*0000-10000000,$FISHPOOL,[0]It's just a regular fish."
  Data.s "TORCH,!LIGHT,!EXTINGUISH,!BURN,!SNUFF,!UNLIGHT,*1001-10010000,$INVENTORY,[0]The torch is made from a tree branch with an oil-covered cloth wrapped around one end. [1]The torch is burning strongly.[2]The torch is flickering and burning dimly.[3]The torch is currently not burning.[4]The torch is used up."
  Data.s "BOW,^YEW,!SHOOT,!STRING,!HOLSTER,!PULL,*1000-10000000,$ROBBERSCAVE,@GET:-2:$ROBBERSCAVE,[0]This is a fine yew bow with a stiff\@ waxed string.[1]The bow has been used a bit\@ but is still in good condition.[2]The bow is unusable as the string has snapped."
  Data.s "ARROW,!SHOOT,!NOCK,!KNOCK,!STRING,!FIRE,*1000-00000000,$ROBBERSCAVE,@GET:-2:$ROBBERSCAVE,[0]You have several sharp-tipped arrows with colorful\@ feathered fletching.[9]"
  Data.s "CLERK,!TALK,!SPEAK,!BRIBE,!GIVE,!KISS,!ROB,!FIGHT,!KILL,!ATTACK,!SHOOT,*1100-10000000,$ZARBURGTREASURY,[0]The clerk is an elderly woman with a kind\@ yet earnest and calculating\@ air about her. She sizes you up with a quick glance and waits for you to speak.[1]The clerk is visibly upset and no longer looks friendly. She glares at you and waits for you to speak... or try anything."
  Data.s "GUARD,!TALK,!SPEAK,!BRIBE,!KISS,!FIGHT,!ATTACK,!KILL,!SHOOT,*1100-10000000,$ZARBURGTREASURY,[0]The royal guards are stoic and unmoving\@ yet undeniably lethal. You'd best not try anything.[1]The guards are at full alert\@ and hover over you with anger in their eyes."
  Data.s "MERCHANT,^SALESMAN,!TALK,!SPEAK,!PAY,!FIGHT,!KILL,!ATTACK,!SHOOT,*1100-11000000,$ZARBURGCOURTYARD,@PAY:-1+3,@PAY:-1:^MEALBAR,@PAY:-1+2:$ZARBURGCOURTYARD,@FIGHT:-0+2,@FIGHT:-2+3:$ZARBURGCOURTYARD,[0]The merchant looks at you with wary eyes\@ but a friendly demeanor. [1]He wants to sell something to every customer![2]The merchant is very cool towards you and is talking about calling the guards.[3] His shop is ready to close and only one meal bar remains."
  Data.s "MEALBAR,^BAR,!BUY,!PURCHASE,!STEAL,!EAT,*0000-11000000,$ZARBURGCOURTYARD,@BUY:-1,@BUY:-1+3:^MERCHANT,@BUY:-1+2:$ZARBURGCOURTYARD,@STEAL:-0+2:^MERCHANT,@STEAL:-2+3:$ZARBURGCOURTYARD,[0]The mealbar looks hearty and savory. One should feed you for a full day.[1]The price is one coin per bar."
  Data.s "ELF,^DROW,!TALK,!SPEAK,!BARGAIN,!FIGHT,!ATTACK,!KILL,!SHOOT,!KISS,!ROB,!STEAL,!PAY,*0100-10000000,$ZARBURGINN,[0]This drow looks quite dangerous. She is dressed in all black\@ adding to her menacing persona.[1]The dark elf stands in a defensive posture\@ and you can tell that she know what she is about."
  Data.s "TRAP,!DISARM,!DETECT,!SPRING,!TRIP,*0109-00000000,$ROBBERSCAVE,[0]A tripwire spans the hallway\@ ready to shoot poisonous darts from the walls![1]A disarmed tripwire lays harmless against one wall."
  Data.s "COIN,^GOLD,^MONEY,!GIVE,!USE,*1001-11000000,$INVENTORY,[1]This is a[2]These are[0] solid gold coin[2]s[0] with the royal seal of Zarburg stamped on [1]it[2]them[0]."
  Data.s "WATCHMAN,!TALK,!SPEAK,!FIGHT,!ATTACK,!KILL,!SHOOT,!BRIBE,!PAY,*0100-10000000,$ZARBURGSOUTHGATE,@BRIBE:-1+2-6:$ZARBURGSOUTHGATE,@BRIBE:-1+2-3:^GATE,@BRIBE:-0+1,[0]The watchman is an older fellow\@ spry and wiry\@ with gray hair and shifty eyes. He looks hungry in every sense of the word.[1]The watchman is rubbing his hands excitedly and mumbling to himself."
  Data.s "TOOLS,^LOCKPICK,!USE,*1000-10000000,$ROBBERSCAVE,@GET:-1:$ROBBERSCAVE,[0]This is a fine set of lock picking tools\@ they certainly can unlock any lock you encounter."
  Data.s "BACKPACK,^PACK,*1001-10100000,$INVENTORY,[0]It's a rugged backpack that contains the items you are carrying.[1] The backpack is currently empty.[2] The pack is starting to fill up.[3] The pack is over halfway full.[4] Your backpack is full. You'll have to drop something to carry more."
  Data.s "<x>"
EndDataSection

;(x,y) = location in room grid array
;NSEW positions: 0=no,1=yes,2=locked,3=closed,5=blocked,6=teleport,9=codehandler <--these numbers help us draw the map
;normally dark, 1 or 0 + initial room state: 0-7.
;strings based on [state] value. If [n] state is set, [n] string will be printed.  [*] is string for "dark" state.

DataSection
  __RoomList__:
  Data.s "INVENTORY" ;reserved for player inventory--this fake room holds all items that user is carrying
  Data.s "ITEMGONE"  ;reserved for items that are destroyed or otherwise lost by player
  Data.s "TEMPSTORAGE"  ;reseved for temporary location to put inventory items when backpack is dropped
  Data.s "ROBBERSCAVE,(0.0),^0010,*111100000,[*]You're in a dark cave.[0]This small cave is used by a band of robbers.[1] There are a set of lockpicking tools hanging from a hook on the wall.[2] A fine yew bow and several arrows lay in a dark corner[1]."
  Data.s "EMPTYROAD,(1.0),^0011,*010000000,[0]$An empty road."
  Data.s "ZARBURGROAD,(2.0),^0101,*010000000,[0]$Crossroads in and out of Zarburg."
  ;Data.s "ZARBURGROAD,(2.0),^0111,*010000000,[0]$Crossroads in and out of Zarburg."
  Data.s "ZARBURGTREASURY,(0.1),^0010,*010000000,[0]This is the Zarburg treasury. The royal clerk stands behind a polished desk and two royal guards stand at attention in front of a large vault."
  Data.s "ZARBURGTHRONEROOM,(1.1),^0101,*010000000,[0]$The Zarburg throne room."
  Data.s "ZARBURGBRIDGE,(2.1),^1100,*010000000,[0]$The bridge connecting Zarburg to the road."
  Data.s "ZARBURGINN,(0.2),^0010,*011100000,[0]You are in the Green Dragon Inn\@ a dimly lit\@ seedy establishment on the far west side of town.[1] A cloaked dark elf [2]sits alone at a corner table.[3]stands menacingly near a corner table.[0] A young barmaid stands behind the bar\@ looking out the door to the east."
  Data.s "ZARBURGCOURTYARD,(1.2),^1111,*011000000,[0]This is the main courtyard of Zarburg\@ drab and without decoration. [1]There is a merchant here\@ selling meal bars.[2]A merchant here gives you a nod and looks apologetically at his empty trays.[3]The merchant fixes you with an angry state\@ his hand resting on his trusty blunderbuss.[0] There is an alleyway leading east and buildings toward the north and west."
  Data.s "ZARBURGALLEYWAY,(2.2),^1101,*010000000,[0]$Alleyway between armory\@ courtyard\@ and north gate."
  Data.s "ZARBURGCLIFF,(0.3),^0010,*010000000,[0]The western edge of Zarburg drops off sharply. The cliff here is wet and quite slippery. You can't see all the way down through the clouds. Danger![1] There is a small opening through some branches to the south."
  Data.s "ZARBURGSOUTHGATE,(1.3),^2001,*011011000,[0]The Zarburg town gate stands tall immediately to the north; [1]the gate is locked closed[6] but a watchman is sizing you up through a small window[1].[2]the gate is open and the watchman standing here looks very content.[3] There is a large oak tree nearby[4] with an axe leaning against it[3].[5] A tall oak tree with an axe embedded in it lies crashed in front of the gate.[0] A narrow path leads toward a cliff to the west.,%[0]Welcome to Eldarian Odyssey\@ an early 80's based text adventure game. Use two word commands to interact with the world\@ discover the objective\@ and win the game! Type HELP for more assistance.^^You are carrying a backpack and an unlit torch."
  Data.s "ZARBURGARMORY,(2.3),^1000,*110000000,[*]It's very dark in here\@ with a faint metallic odor.[0]$You're in the Zarburg armory. Lots of pretty things are here."
  ;Data.s "ZARBURGARMORY,(2.3),^1000,*010000000,[*]It's very dark in here\@ with a faint metallic odor.[0]$You're in the Zarburg armory. Lots of pretty things are here."
  Data.s "WOODSBURIED,(4.1),^1101,*111000000,[*]You feel trees And brush all around you but it's too dark to make things out.[0]There are a variety of trees all around you in these dense woods.[1] You briefly stumble over a bottle that is mostly buried near the base of a singular[2] You stand in front of a lone[0] birch tree."
  Data.s "WOODSTREE,(4.2),^1001,*110000000,[*]It feels less crowded here\@ but you definitely feel bark.[0]A giant fir tree stands tall above the rest of the forest."
  Data.s "FISHPOOL,(6.1),^1100,*010000000,[0]There is a deep\@ round pool here. You can see a few fish swimming in it.[1] A rickety fishing pole leans against the west wall."
  Data.s "OAKROOM,(7.0),^0000,*011100000,[0]You are high up in the oak tree.[1] Through the branches you can see over the gate to the town of Zarburg.[2] There is a watchman sleeping in a worn-down chair near the gate."
  Data.s "BIRCHROOM,(7.1),^0000,*110000000,[*]It's dark up here@\ you can barely see your hands in the dim light that reaches here from above.[0]You have climbed up into the old birch tree. From here you can see several other trees\@ all tightly packed together."
  Data.s "FIRROOM,(7.2),^0000,*010000000,[0]You are about midway up the ancient fir. The sunlight reaches here\@ so you can see a little. Around you are dozens of trees\@ all clumped together. The branches are much thinner above you."
  Data.s "FIRROOM2,(7.3),^0000,*010000000,[0]You are in an open spot near the top of the tree. Below you to the southwest you can see a trail leading out of the woods."
  Data.s "<x>"
EndDataSection
; IDE Options = PureBasic 5.62 (Windows - x64)
; CursorPosition = 25
; EnableXP